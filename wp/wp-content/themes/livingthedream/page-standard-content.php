<?php
/*
Template Name: Main Page Layout
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">

		<div id="main">
			<div class="left-column">
				
					<h2><?php the_title();?></h2>
					<div class="main-content">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, nisi, quos, ex reprehenderit unde delectus vel amet ipsam temporibus eius officiis animi dolorem dicta modi aliquid! Ipsam provident animi maiores.</p>						
					</div>

					<?php if(is_page('contact')):?>
						<div class="contact-block">
							<div class="primary-contact clearfix">
								<h3>General Enquiries</h3>
								<div class="address">
									<span>Write To Us:</span>
									Unit 36<br />
									88-90 Hatton Garden<br /> London<br /> EC1N 8PN
								</div>
								<ul>
									<span class="title">Alternative Contact Details:</span>
									<li>Email: <a href="mailto:info@livingthedreamcompany.co.uk">info@livingthedreamcompany.co.uk</a></li>
									<li>Tel: <span>01442 381164</span></li>
								</ul>
							</div>

							<aside class="contact">
								<h3>School</h3>
								<p>For all School enquiries...</p>
								<ul>
									<li><a href="mailto:info@livingthedreamcompany.co.uk">Email us</a></li>
									<li><span>01442 381164</span></li>
								</ul>
							</aside>
							<aside class="contact">
								<h3>Company</h3>
								<p>For all School enquiries...</p>
								<ul>
									<li><a href="mailto:Email us">Email us</a></li>
									<li><span>01442 381164</span></li>
								</ul>
							</aside>							
							<aside class="contact">
								<h3>Foundation</h3>
								<p>For all School enquiries...</p>
								<ul>
									<li><a href="mailto:Email us">Email us</a></li>
									<li><span>01442 381164</span></li>
								</ul>
							</aside>
							<aside class="contact">
								<h3>Agency</h3>
								<p>For all School enquiries...</p>
								<ul>
									<li><a href="mailto:Email us">Email us</a></li>
									<li><span>01442 381164</span></li>
								</ul>
							</aside>
						</div>
					<?php endif; ?>

			</div>

			<div class="right-column">
				<aside class="post clearfix">
					<h3>Lorem ipsum dolor</h3>
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="excerpt">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum, perferendis, eius, ea tempora veritatis magni porro a eligendi repudiandae soluta quaerat deleniti?</p>
					</div>

					<a href="#" class="read-more">Read More...</a>
				</aside>
				<aside class="post clearfix">
					<h3>Lorem ipsum dolor</h3>
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="excerpt">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum, perferendis, eius, ea tempora veritatis magni porro a eligendi repudiandae soluta quaerat deleniti?</p>
					</div>

					<a href="#" class="read-more">Read More...</a>
				</aside>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
