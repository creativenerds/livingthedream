<?php
/*
Template Name: Overview Page
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix timeline">
	<div class="inner clearfix">

		<div id="our-timeline"></div>

		<div class="left-column">

				<h2><?php the_title();?></h2>
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam, hic, in incidunt repudiandae debitis ab rem nesciunt dolor ducimus unde dignissimos illo consequuntur nostrum expedita sed iste pariatur harum esse.</p>
				<p>Distinctio natus voluptates vitae ipsam ut? Nostrum eveniet maiores asperiores perferendis repellat cupiditate? Quidem, dolore, ex, sit accusantium veniam illum assumenda praesentium veritatis cupiditate sapiente consequatur molestiae enim libero voluptate.</p>
				<p>Soluta, beatae, necessitatibus, nihil hic omnis neque nisi sed debitis delectus earum unde ratione modi dignissimos animi quibusdam quos veniam! Sit, iure quibusdam enim a non iusto libero voluptate cum.</p>
				<p>Similique sequi earum assumenda nemo laboriosam asperiores tempore! Laudantium, recusandae, natus, et quisquam tempora provident odio perferendis velit unde assumenda molestias in reiciendis obcaecati libero deserunt accusantium sapiente consequatur reprehenderit.</p>
				<p>Eum, quo, iusto, autem nam ratione neque maiores quam quibusdam facilis hic aliquam tenetur suscipit veritatis ipsa commodi nisi ipsum error. Repudiandae quam cumque iusto enim non ratione dolore est.</p>
				
		</div>

		<div class="right-column">
				<aside class="twitter clearfix">
					<h2>TWITTER</h2>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>
					</ul>
				</aside>

				<aside class="facebook clearfix">
					<h2>FACEBOOK</h2>
					<p>Living The Dream Foundation is on facebook. Keep up to date with the latest news and events.</p>
					<a href="#">Like Us On Facebook +</a>
				</aside>
			</div>

	</div>
</div>

<?php get_footer(); ?>
