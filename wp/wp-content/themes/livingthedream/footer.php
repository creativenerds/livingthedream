					</div> <!-- end #container -->

			<footer class="footer-section">
				<div class="inner">
					<div class="block">
						<h3>Find Us</h3>
						<ul class="social">
							<li><a class="facebook" href="#"><span></span>Like us on Facebook</a></li>
							<li><a class="twitter" href="#"><span></span>Follow us on Twitter</a></li>
							<li><a class="linkedin" href="#"><span></span>Find Us on Linked In</a></li>
							<li><a class="youtube" href="#"><span></span>Watch Us on Youtube</a></li>
						</ul>
					</div>
					<div class="block contact">
						<h3>Contact Us</h3>
						<span>Email:</span>
						<a href="mailto:info@livingthedreamcompany.co.uk">info@livingthedreamcompany.co.uk</a>

						<span>Telephone</span>
						<a>+44 (0) 7845 267 501</a>

						<h3>Subscribe</h3>
						<form action="">
							<input type="text">
							<button>></button>
						</form>
					</div>					
					<div class="block contact">
						<h3>Testimonials</h3>
						<div class="testimonial-content">
							"This is a testimonial relating to the Living The Dream Foundation, great work"
						</div>
					</div>
				</div>
			</footer>


		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page. what a ride! -->
