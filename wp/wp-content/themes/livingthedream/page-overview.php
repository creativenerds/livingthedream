<?php
/*
Template Name: Overview Templates
*/
?>

<?php get_header(); ?>
<div id="main-container" class="clearfix overview">
	<div class="inner clearfix">
		<div class="sub-navigation clearfix">
			<nav class="clearfix">
				<?php wp_nav_menu( array('theme_location' => 'school' )); ?>
			</nav>
		</div>

		<div id="overview-introduction">
			<img src="http://placehold.it/450x300" class="align-left" alt="">

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, quisquam recusandae repellat ut earum ab id nesciunt vel sed vero maxime dolorem quae amet dignissimos accusantium ipsum saepe facere optio.</p>

			<p>Reiciendis, harum, architecto, quam, omnis natus accusamus odit perspiciatis cum quod blanditiis ab aspernatur nulla ad quia quibusdam eius nobis aliquam nisi tempore aut facere dolor tenetur quaerat quas facilis.</p>
			<p>Nobis, cum, beatae, ad aspernatur animi aut voluptas adipisci mollitia corporis ducimus perspiciatis nesciunt quo nulla recusandae reprehenderit natus error! Sequi, natus repellendus porro ipsa magnam laudantium a autem consequuntur!</p>
			<p>Modi, ratione sint possimus obcaecati dolorem magni odit explicabo voluptatibus. Sequi minima minus illum. Iusto, iure, aperiam, architecto suscipit nulla tenetur consequuntur nobis beatae velit deserunt facilis maxime! Dolor, repudiandae.</p>
		</div>
	</div>
</div>

<?php get_footer(); ?>
