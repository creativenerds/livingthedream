<?php get_header(); ?>

<section id="category-grid" class="clearfix image-block">
	<ul>
		<li class="cat-block">
			<a href="#">
				<img src="<?php bloginfo('template_url');?>/library/images/school-bw.png" alt="">
				<img src="<?php bloginfo('template_url');?>/library/images/school.png" alt="" class="active">
				<div class="content-overlay">
					<span class="banner">SCHOOL OF PERFORMING ARTS</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident blanditiis voluptates culpa saepe perspiciatis quas! Ducimus, illum, unde, laborum necessitatibus quaerat sunt repellat quas eum nam asperiores maiores ad quod!</p>
				</div>
			</a>
		</li>
		<li class="cat-block">
			<a href="#">
				<img src="<?php bloginfo('template_url');?>/library/images/dance-co-bw.png" alt="">
				<img src="<?php bloginfo('template_url');?>/library/images/dance-co.png" alt="" class="active">
				<div class="content-overlay">
					<span class="banner">DANCE COMPANY</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident blanditiis voluptates culpa saepe perspiciatis quas! Ducimus, illum, unde, laborum necessitatibus quaerat sunt repellat quas eum nam asperiores maiores ad quod!</p>
				</div>
			</a>
		</li>
		<li class="cat-block">
			<a href="#">
				<img src="<?php bloginfo('template_url');?>/library/images/agency-bw.png" alt="">
				<img src="<?php bloginfo('template_url');?>/library/images/agency.png" alt="" class="active">
				<div class="content-overlay">
					<span class="banner">TALENT AGENCY</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident blanditiis voluptates culpa saepe perspiciatis quas! Ducimus, illum, unde, laborum necessitatibus quaerat sunt repellat quas eum nam asperiores maiores ad quod!</p>
				</div>
			</a>
		</li>

		<li class="cat-block">
			<a href="#">
				<img src="<?php bloginfo('template_url');?>/library/images/foundation-bw.png" alt="">
				<img src="<?php bloginfo('template_url');?>/library/images/foundation.png" alt="" class="active">
				<div class="content-overlay">
					<span class="banner">DREAM FOUNDATION</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident blanditiis voluptates culpa saepe perspiciatis quas! Ducimus, illum, unde, laborum necessitatibus quaerat sunt repellat quas eum nam asperiores maiores ad quod!</p>
				</div>
			</a>
		</li>
	</ul>
</section>

<div class="vid-block blocks">
	<h3><span>Videos</span></h3>
	<?php echo get_new_royalslider(2);?>
</div>

<div class="latest-news blocks">
	<h3><span>News</span></h3>
	<ul>
		<li class="clearfix first">
			<figure>
				<img src="http://placehold.it/96x68" alt="">
			</figure>
			<div class="about-news">
				<span>Lorem Ipsum dolor sit</span>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing eli Donec gravida nunc porttitor tellus...</p>
			</div>
		</li>
		<li class="clearfix">
			<figure>
				<img src="http://placehold.it/96x68" alt="">
			</figure>
			<div class="about-news">
				<span>Lorem Ipsum dolor sit</span>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing eli Donec gravida nunc porttitor tellus...</p>
			</div>
		</li>
		<li class="clearfix">
			<figure>
				<img src="http://placehold.it/96x68" alt="">
			</figure>
			<div class="about-news">
				<span>Lorem Ipsum dolor sit</span>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing eli Donec gravida nunc porttitor tellus...</p>
			</div>
		</li>
		<li class="clearfix">
			<figure>
				<img src="http://placehold.it/96x68" alt="">
			</figure>
			<div class="about-news">
				<span>Lorem Ipsum dolor sit</span>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing eli Donec gravida nunc porttitor tellus...</p>
			</div>
		</li>
	</ul>
</div>

<div class="blocks twitters">
	<h3><span>Tweets</span></h3>
	<div class="tweet-box"><a class="twitter-timeline" href="https://twitter.com/LivinTheDreamCo" data-widget-id="345972981903798272">Tweets by @LivinTheDreamCo</a></div>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>

<div class="blocks video-flash">
	<h3><span>Flashmob</span></h3>
	<div class="video">
		<iframe width="435" height="270" src="http://www.youtube.com/embed/-bnYpCiwV2Q" frameborder="0" allowfullscreen></iframe>
	</div>
</div>

<?php get_footer(); ?>
