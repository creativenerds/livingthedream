<?php
/*
Template Name: 4 Col Layout
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">
		<div class="sub-navigation clearfix">
			<nav class="clearfix">
				<?php wp_nav_menu( array('theme_location' => 'school' )); ?>
			</nav>
		</div>

		<div class="introduction-section">
			<h2><?php the_title();?></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, nisi error optio accusantium dolores eaque natus explicabo veritatis voluptate veniam? Officia, adipisci voluptatum quod quas quam temporibus iste sapiente consectetur.</p>
		</div>
		<div id="main">


			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
			<article class="class clearfix">
				<h3>Lorem ipsum dolor</h3>
				<figure>
					<img src="http://placehold.it/268x130" alt="">
				</figure>
				<div class="excerpt">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
				</div>

				<a href="#" class="read-more">Find Out More...</a>
			</article>
		</div>

	</div>
</div>

<?php get_footer(); ?>
