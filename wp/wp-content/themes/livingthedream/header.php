<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>

		<header class="main-header clearfix">
			<div class="inner clearfix">
				<div class="sub-contact">
					<div class="telephone">
						<span>CALL:</span>+44 (0) 1234 567890
					</div>
					<div class="email">
						<span>EMAIL:</span><a href="mailto:info@livingthedreamcompany.co.uk">info@livingthedreamcompany.co.uk</a>
					</div>
				</div>

				<a href="/" class="logo"></a>

				<div class="newsletter-signup">
					<form action="">
						<label for="signup">Stay up to date...</label>
						<input type="text" name="signup" placeholder="Enter email address...">
						<button>GO</button>
					</form>
				</div>
				<div class="nav-section clearfix">
					<nav class="left-nav clearfix">
						<?php wp_nav_menu( array('theme_location' => 'left-nav' )); ?>
					</nav>
					<nav class="right-nav clearfix">
						<?php wp_nav_menu( array('theme_location' => 'right-nav' )); ?>
					</nav>
				</div>
			</div>
		</header>

		<div id="container" class="clearfix">
			
			<?php if(is_front_page()):?>
			<div id="brand-section" class="clearfix">
				<div id="slider">
					<?php echo get_new_royalslider(1); ?>
				</div>
			</div>
		<?php endif; ?>

