<?php
/*
Template Name: Overview Page
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">
		
		<div class="introduction-section">
			<h2>LATEST NEWS</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, nisi error optio accusantium dolores eaque natus explicabo veritatis voluptate veniam? Officia, adipisci voluptatum quod quas quam temporibus iste sapiente consectetur.</p>
		</div>

		<div id="news-section">
			<div class="left-column single">
				
				<article >
					<h2><?php the_title();?></h2>
					<figure>
						<img src="http://placehold.it/552x297" alt="">
					</figure>
					<div class="description-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, accusantium, beatae, accusamus, dolore nesciunt voluptatem error quasi eos unde at maiores atque placeat ipsam sed repudiandae esse recusandae ipsum iste?</p>
						<p>Architecto, similique, voluptate, nemo dolor dolorem repellendus perspiciatis esse animi laboriosam molestias eaque odit provident ad fugiat quibusdam iure hic excepturi illum! Sunt, consectetur soluta nam vel fuga mollitia omnis.</p>
						<p>Voluptates, in, dolor necessitatibus quas nulla dicta aspernatur accusamus at alias odio eveniet officia quibusdam optio molestias odit vitae dolorem laudantium maiores illum praesentium eum fugit blanditiis nobis itaque minus.</p>
						<p>Dolorem, totam, ipsam, animi sapiente pariatur reprehenderit cum quia nesciunt tempore repellendus eius adipisci suscipit sed quisquam voluptatibus! Illum, velit, minus accusamus delectus consequuntur labore pariatur sequi quod nisi quibusdam.</p>
						<p>Quis, distinctio ad eveniet nobis cumque dignissimos impedit accusamus voluptatum? A ratione fugiat quaerat dolore maiores quos asperiores ad officia ullam dignissimos. Culpa, consequatur, quisquam unde maiores repellat sapiente magnam?</p>
						<p>Quam, voluptatum, temporibus, nesciunt, non dolores omnis architecto placeat eaque nam facere enim illo. Aliquam, ea, doloribus, esse expedita eius harum illo recusandae iure laboriosam quibusdam maiores sed dolores quidem.</p>
						<p>Accusantium, ea, illum, asperiores illo quis ratione vitae quae minus beatae fugit cupiditate obcaecati autem consequatur. Natus, amet, cum, aspernatur ipsa labore explicabo facilis nobis quasi debitis fugit similique hic.</p>
						<p>Earum, ratione, maiores laborum hic odit vel libero quisquam quia modi est fugit porro totam tempora rem voluptatum animi sed vero omnis tenetur harum dignissimos perferendis accusantium ullam sapiente voluptatem.</p>
					</div>
				</article>

			</div>

			<div class="right-column">
				<aside class="twitter clearfix">
					<h2>TWITTER</h2>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>
					</ul>
				</aside>

				<aside class="facebook clearfix">
					<h2>FACEBOOK</h2>
					<p>Living The Dream Foundation is on facebook. Keep up to date with the latest news and events.</p>
					<a href="#">Like Us On Facebook +</a>
				</aside>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
