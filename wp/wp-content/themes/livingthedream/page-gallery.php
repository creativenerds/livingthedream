<?php
/*
Template Name: Gallery Layout
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">
		<div class="sub-navigation clearfix">
			<nav class="clearfix">
				<?php wp_nav_menu( array('theme_location' => 'school' )); ?>
			</nav>
		</div>
		<div class="introduction-section">
			<h2>GALLERY</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, nisi error optio accusantium dolores eaque natus explicabo veritatis voluptate veniam? Officia, adipisci voluptatum quod quas quam temporibus iste sapiente consectetur.</p>
		</div>

		<div class="gallery-row">
			<div class="gallery-album clearfix">
				<h3>Lorem Ipsum Dolar</h3>
				<ul class="clearfix">
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
				</ul>
			</div>

			<div class="gallery-video">
				<h3>Lorem Ipsum Dolar</h3>
				<iframe width="445" height="246" src="//www.youtube.com/embed/OCWj5xgu5Ng?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>

		<div class="gallery-row">

			<div class="gallery-video">
				<h3>Lorem Ipsum Dolar</h3>
				<iframe width="445" height="246" src="//www.youtube.com/embed/OCWj5xgu5Ng?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="gallery-album clearfix right">
				<h3>Lorem Ipsum Dolar</h3>
				<ul class="clearfix">
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
					<li><a href="#"><img src="http://placehold.it/75x75" alt=""></a></li>
				</ul>
			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>
