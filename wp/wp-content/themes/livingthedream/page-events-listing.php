<?php
/*
Template Name: Events Listing
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">
		<div class="sub-navigation clearfix">
			<nav class="clearfix">
				<?php wp_nav_menu( array('theme_location' => 'school' )); ?>
			</nav>
		</div>

		<div class="introduction-section">
			<h2><?php the_title();?></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, nisi error optio accusantium dolores eaque natus explicabo veritatis voluptate veniam? Officia, adipisci voluptatum quod quas quam temporibus iste sapiente consectetur.</p>
		</div>
		<div id="main" class="events">

			<div class="left-column">
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
				<article class="class clearfix">
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="event-content">
						<h3>Lorem ipsum dolor</h3>					
						<div class="excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum.</p>
						</div>
						<span class="date">EVENT DATE: 12th September 2013</span>
					</div>
				</article>
			</div>

			<div class="right-column">
				<aside class="post clearfix">
					<h3>Lorem ipsum dolor</h3>
					<figure>
						<img src="http://placehold.it/268x130" alt="">
					</figure>
					<div class="excerpt">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque molestias nobis quam ipsa perferendis repellendus? Nostrum, perferendis, eius, ea tempora veritatis magni porro a eligendi repudiandae soluta quaerat deleniti?</p>
					</div>

					<a href="#" class="read-more">Read More...</a>
				</aside>
				<aside class="twitter clearfix">
					<h2>TWITTER</h2>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
							<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
							<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
							<span>2 Days Ago</span>
						</li>
					</ul>
				</aside>

				<aside class="facebook clearfix">
					<h2>FACEBOOK</h2>
					<p>Living The Dream Foundation is on facebook. Keep up to date with the latest news and events.</p>
					<a href="#">Like Us On Facebook +</a>
				</aside>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
