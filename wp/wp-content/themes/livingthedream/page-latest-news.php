<?php
/*
Template Name: News Layout
*/
?>

<?php get_header(); ?>

<div id="main-container" class="clearfix">
	<div class="inner clearfix">
		<div class="sub-navigation clearfix">
			<nav class="clearfix">
				<?php wp_nav_menu( array('theme_location' => 'school' )); ?>
			</nav>
		</div>
		
		<div class="introduction-section">
			<h2><?php the_title();?></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, nisi error optio accusantium dolores eaque natus explicabo veritatis voluptate veniam? Officia, adipisci voluptatum quod quas quam temporibus iste sapiente consectetur.</p>
		</div>

		<div id="news-section">
			<div class="left-column">
				<article id="featured">
					<h2>Featured Story</h2>
					<figure>
						<img src="http://placehold.it/552x297" alt="">
					</figure>
					<div class="description-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, minus, nobis in maxime blanditiis est fuga quasi necessitatibus nihil dolorem incidunt porro vel impedit nisi culpa laborum eius praesentium debitis!</p>
					</div>
				</article>

				<article>
					<h2>STORY TITLE</h2>
					<figure>
						<img src="http://placehold.it/259x123" alt="">
					</figure>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet fringilla mi sed commodo. Lorem ipsum dolor sit amet.</p>
					<a href="#" class="read-more">Read More +</a>
				</article>

				<article class="odd">
					<h2>STORY TITLE</h2>
					<figure>
						<img src="http://placehold.it/259x123" alt="">
					</figure>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet fringilla mi sed commodo. Lorem ipsum dolor sit amet.</p>
					<a href="#" class="read-more">Read More +</a>
				</article>


				<article>
					<h2>STORY TITLE</h2>
					<figure>
						<img src="http://placehold.it/259x123" alt="">
					</figure>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet fringilla mi sed commodo. Lorem ipsum dolor sit amet.</p>
					<a href="#" class="read-more">Read More +</a>
				</article>

				<article class="odd">
					<h2>STORY TITLE</h2>
					<figure>
						<img src="http://placehold.it/259x123" alt="">
					</figure>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet fringilla mi sed commodo. Lorem ipsum dolor sit amet.</p>
					<a href="#" class="read-more">Read More +</a>
				</article>

			</div>

			<div class="right-column">
				<aside class="twitter clearfix">
					<h2>TWITTER</h2>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>

						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, ea, reprehenderit.
						<span>2 Days Ago</span>
						</li>
					</ul>
				</aside>

				<aside class="facebook clearfix">
					<h2>FACEBOOK</h2>
					<p>Living The Dream Foundation is on facebook. Keep up to date with the latest news and events.</p>
					<a href="#">Like Us On Facebook +</a>
				</aside>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
