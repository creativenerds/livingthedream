network-wide-menu
=================

Network Wide Menu for WordPress Networks

Install in either the mu-plugins folder or network activate from the plugins folder

This plugin caches a copy of the menu items from the first registered navigation menu on the main site in the network.

The cached menu items are used to replace the contents of the first registered navigation menu on all sub sites on the network.

Subsite Requirements:

- The sub site must have at least one navigation menu with one menu item before the filters used by this plugin come into effect.